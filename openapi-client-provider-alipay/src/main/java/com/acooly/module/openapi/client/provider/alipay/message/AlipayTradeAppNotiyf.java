package com.acooly.module.openapi.client.provider.alipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayApiMsgInfo;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayNotify;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;
import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

@AlipayApiMsgInfo(service=AlipayServiceEnum.PAY_ALIPAY_TRADE_APP,type=ApiMessageType.Notify)
@Setter
@Getter
public class AlipayTradeAppNotiyf extends AlipayNotify{

	/** 交易结束时间 格式为yyyy-MM-dd HH:mm:ss*/
	@AlipayAlias(value = "gmt_close")
	private String gmtClose;
	/** 交易付款时间 格式为yyyy-MM-dd HH:mm:ss*/
	@AlipayAlias(value = "gmt_payment")
	private String gmtPayment;
	/** 订单总金额，与请求中的订单金额一致 */
	@AlipayAlias(value = "total_amount")
	private String totalAmount;
	/** 实收金额，商户实际入账的金额（扣手续费之前） */
	@AlipayAlias(value = "receipt_amount")
	private String receiptAmount;
}
