package com.acooly.module.openapi.client.provider.wft;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.notify.WftApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.wft.OpenAPIClientWftProperties.PREFIX;


@EnableConfigurationProperties({OpenAPIClientWftProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientWftConfigration {

    @Autowired
    private OpenAPIClientWftProperties openAPIClientWftProperties;

    @Bean("wftHttpTransport")
    public Transport WftHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientWftProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientWftProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientWftProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 易行通SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        WftApiServiceClientServlet apiServiceClientServlet = new WftApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "wftNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add("/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_JSPAY.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wftNotify/" + WftServiceEnum.PAY_ALIPAY_NATIVE.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_NATIVE.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_RAW_APP.getKey());//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }
}
