/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.baofup.domain;

import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * Fuiou 响应报文基类
 *
 * @author zhike
 */
@Getter
@Setter
public class BaoFuPResponse extends BaoFuPApiMessage {

    /**
     * 数字信封（请求不用传）
     * 格式：01|对称密钥，01代表AES
     * 加密方式：Base64转码后使用宝付的公钥加密
     */
    @Size(max = 512)
    @BaoFuPAlias(value = "dgtl_envlp")
    private String dgtlEnvlp;

    /**
     * 报文流水号
     */
    @NotBlank
    @Size(max = 32)
    @BaoFuPAlias(value = "msg_id")
    private String msgId;

    /**
     * 返回状态码
     */
    @BaoFuPAlias("resp_code")
    private String respCode;

    /**
     * 业务返回码
     */
    @BaoFuPAlias("biz_resp_code")
    private String bizRespCode;

    /**
     * 业务返回说明
     */
    @BaoFuPAlias("biz_resp_msg")
    private String bizRespMsg;
}
