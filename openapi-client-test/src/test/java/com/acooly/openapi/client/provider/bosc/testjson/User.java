/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 22:55 创建
 */
package com.acooly.openapi.client.provider.bosc.testjson;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author zhangpu 2017-11-15 22:55
 */
@Getter
@Setter
@ToString
public class User {

    private Long id;

    private String name;

    public User() {
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

}
