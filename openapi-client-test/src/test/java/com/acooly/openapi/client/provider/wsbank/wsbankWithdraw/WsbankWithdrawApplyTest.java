package com.acooly.openapi.client.provider.wsbank.wsbankWithdraw;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.*;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 单笔提现申请
 * @author sunjx
 *
 */
@SpringBootApplication
@BootApp(sysName = "wsbankWithdrawApplyTest")
public class WsbankWithdrawApplyTest extends NoWebTestBase {

    @Autowired
    private WsbankApiService wsbankApiService;

    /**
     * 单笔提现申请
     */
    @Test
    public void withdrawApplyTest() {
        WsbankWithdrawApplyRequestBody body = new WsbankWithdrawApplyRequestBody();
        body.setMerchantId("226801000000114869124");
        body.setOutTradeNo(Ids.oid());
        body.setPlatformFee("0");//平台设置提现手续费用
        body.setTotalAmount("2");
        body.setMemo("提现");
        WsbankWithdrawApplyRequestInfo requestInfo = new WsbankWithdrawApplyRequestInfo();
        requestInfo.setWsbankWithdrawApplyRequestBody(body);
        WsbankWithdrawApplyRequest request = new WsbankWithdrawApplyRequest();
        request.setRequestInfo(requestInfo);
        WsbankWithdrawApplyResponse response = wsbankApiService.withdrawApply(request);
        System.out.println("测试提现结果为:" + JSON.toJSONString(response));
    }
    
    /**
     * 商户单笔提现确认接口
     */
    @Test
    public void ordershareQuery() {
    	WsbankWithdrawConfirmRequestBody requestBody = new WsbankWithdrawConfirmRequestBody();
    	requestBody.setMerchantId("226801000000114869124");//提现商户号
    	requestBody.setOutTradeNo("o18070316013299440001");//外部订单请求流水号
    	requestBody.setOrderNo("201807120300006096");//网商支付订单号
    	requestBody.setPlatformFee("0");//平台设置提现手续费用
    	requestBody.setTotalAmount("2");//订单金额
    	requestBody.setSmsCode("888888");//短信动态码
    	requestBody.setMemo("提现确认");
    	WsbankWithdrawConfirmRequestInfo requestInfo = new WsbankWithdrawConfirmRequestInfo();
    	requestInfo.setRequestBody(requestBody);
    	WsbankWithdrawConfirmRequest request = new WsbankWithdrawConfirmRequest();
        request.setRequestInfo(requestInfo);
        WsbankWithdrawConfirmResponse response = wsbankApiService.withdrawConfirm(request);
        System.out.println("测试提现确认结果为:" + JSON.toJSONString(response));
    }
    
    /**
     * 单笔提现查询接口
     */
    @Test
    public void withdrawQueryTest() {
        WsbankWithdrawQueryRequestBody body = new WsbankWithdrawQueryRequestBody();
        body.setMerchantId("226801000000114869124");
        body.setOutTradeNo("o18070320445092120002");
        body.setOrderNo("201807120300008766");
        WsbankWithdrawQueryRequestInfo requestInfo = new WsbankWithdrawQueryRequestInfo();
        requestInfo.setBody(body);
        WsbankWithdrawQueryRequest request = new WsbankWithdrawQueryRequest();
        request.setRequestInfo(requestInfo);
        WsbankWithdrawQueryResponse response = wsbankApiService.withdrawQuery(request);
        System.out.println("测试提现查询结果为:" + JSON.toJSONString(response));
    }
}
