package com.acooly.openapi.client.provider.wsbank.balance;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankPayeeTypeEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.*;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 余额支付
 * @author sunjx
 *
 */
@SpringBootApplication
@BootApp(sysName = "wsbankOpenPayTest")
public class WsbankOpenPayTest extends NoWebTestBase {

    @Autowired
    private WsbankApiService wsbankApiService;
    
    private static final String merchantId = "226801000000114869124";

    /**
     * 开通余额支付
     */
    @Test
    public void openPayTest() {
        WsbankOpenPayRequestBody body = new WsbankOpenPayRequestBody();
        body.setMerchantId(merchantId);//商户号
        body.setOutTradeNo(Ids.oid());//外部交易号
        WsbankOpenPayRequest request = new WsbankOpenPayRequest();
        WsbankOpenPayRequestInfo requestInfo = new WsbankOpenPayRequestInfo();
        requestInfo.setWsbankOpenPayRequestBody(body);
        request.setWsbankOpenPayRequestInfo(requestInfo);
        WsbankOpenPayResponse response = wsbankApiService.openPay(request);
        System.out.println("测试开通商户余额支付能力结果为:" + JSON.toJSONString(response));
    }
    
    /**
     * 余额支付创建
     */
    @Test
    public void createBalancePayTest() {
        WsbankCreateBalancePayRequestBody body = new WsbankCreateBalancePayRequestBody();
        body.setPayerMerchantId(merchantId);//付款方商户号
        body.setPayeeId("202210000000000001234");//收款方Id
        body.setPayeeType(WsbankPayeeTypeEnum.PLATFORM.getCode());//收款方类型
        body.setOutTradeNo(Ids.oid());//外部订单请求流水号
        body.setTotalAmount("1");//订单金额
        body.setBody("测试点名-测试类目");
        WsbankCreateBalancePayRequestInfo requestInfo = new WsbankCreateBalancePayRequestInfo();
        requestInfo.setWsbankCreateBalancePayRequestBody(body);
        WsbankCreateBalancePayRequest request = new WsbankCreateBalancePayRequest();
        request.setWsbankCreateBalancePayRequestInfo(requestInfo);
        WsbankCreateBalancePayResponse response = wsbankApiService.createBalancePay(request);
        System.out.println("创建余额支付返回结果为:" + JSON.toJSONString(response));
    }
    
    /**
     * 余额支付确认
     */
    @Test
    public void payConfirmTest() {
        WsbankBalancePayConfirmRequestBody body = new WsbankBalancePayConfirmRequestBody();
        body.setPayerMerchantId(merchantId);//付款方商户号
        body.setPayeeId("202210000000000001234");//收款方Id
        body.setPayeeType(WsbankPayeeTypeEnum.PLATFORM.getCode());//收款方类型
        body.setOutTradeNo(Ids.oid());//外部订单请求流水号
        body.setOrderNo("201807850200000001");//网商支付订单号
        body.setTotalAmount("1");//订单金额(金额为分)
        body.setSmsCode("888888");//短信动态码
        WsbankBalancePayConfirmRequestInfo requestInfo = new WsbankBalancePayConfirmRequestInfo();
        requestInfo.setWsbankBalancePayConfirmRequestBody(body);
        WsbankBalancePayConfirmRequest request = new WsbankBalancePayConfirmRequest();
        request.setWsbankBalancePayConfirmRequestInfo(requestInfo);
        WsbankBalancePayConfirmResponse confirmResponse = wsbankApiService.balancePayConfirm(request);
        System.out.println("测试支付确认响应结果为:" + JSON.toJSONString(confirmResponse));
    }
    
    /**
     * 余额支付查询
     */
    @Test
    public void wsbankbalancePayQueryTest() {
        WsbankBalancePayQueryRequestBody body = new WsbankBalancePayQueryRequestBody();
        body.setPayerMerchantId(merchantId);//付款方商户号
        body.setPayeeId("202210000000000001234");//收款方Id
        body.setPayeeType(WsbankPayeeTypeEnum.PLATFORM.getCode());//收款方类型
        body.setOutTradeNo("o18070219572835440001");//外部订单请求流水号
        body.setOrderNo("201807850200000001");//网商支付订单号
        WsbankBalancePayQueryRequestInfo requestInfo = new WsbankBalancePayQueryRequestInfo();
        requestInfo.setWsbankBalancePayQueryRequestBody(body);
        WsbankBalancePayQueryRequest request = new WsbankBalancePayQueryRequest();
        request.setWsbankBalancePayQueryRequestInfo(requestInfo);
        WsbankBalancePayQueryResponse response =  wsbankApiService.balancePayQuery(request);
        System.out.println("测试余额支付查询返回结果为:" + response);
    }
}
