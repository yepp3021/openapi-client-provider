package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawQuerySubDataInfo;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawResponseHead;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawSubDataInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/2/2 18:08
 */
@Getter
@Setter
@XStreamAlias("trans_content")
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.WITHDRAW_QUERY,type = ApiMessageType.Response)
public class BaoFuWithdrawQueryResponse extends BaoFuResponse {
    /**
     * 提现响应头信息
     */
    @XStreamAlias("trans_head")
    private BaoFuWithdrawResponseHead withdrawResponseHead;

    /**
     * 批量提现响应子订单报文
     */
    @XStreamAlias("trans_reqDatas")
    private BaoFuWithdrawQuerySubDataInfo withdrawQuerySubDataInfo;
}
