/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum YinShengTradeStatusEnum implements Messageable {

    WAIT_BUYER_PAY("WAIT_BUYER_PAY", "交易创建，等待买家付款。"),
    TRADE_CLOSED("TRADE_CLOSED", "在指定时间段内未支付时关闭的交易；客户主动关闭订单。"),
    TRADE_SUCCESS("TRADE_SUCCESS", "交易成功，且可对该交易做操作，如：多级分润、退款等。"),
    TRADE_PART_REFUND("TRADE_PART_REFUND", "部分退款成功。"),
    TRADE_ALL_REFUND("TRADE_ALL_REFUND", "全部退款成功。"),
    TRADE_PENDING("TRADE_PENDING","等待卖家收款"),
    TRADE_FINISHED("TRADE_FINISHED","交易成功且结束，即不可再做任何操作。");
    ;

    private final String code;
    private final String message;

    private YinShengTradeStatusEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (YinShengTradeStatusEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YinShengTradeStatusEnum find(String code) {
        for (YinShengTradeStatusEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YinShengTradeStatusEnum> getAll() {
        List<YinShengTradeStatusEnum> list = new ArrayList<YinShengTradeStatusEnum>();
        for (YinShengTradeStatusEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YinShengTradeStatusEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
