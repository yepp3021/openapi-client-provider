package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/2/8 13:59
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.WITHDRAW_D0, type = ApiMessageType.Response)
public class YinShengWithdrawD0Response extends YinShengResponse{
    /**
     *订单号
     */
    private String out_trade_no;

    /**
     *状态码
     */
    private String trade_status;

    /**
     * 状态描述
     */
    private String trade_status_description;

    /**
     * 提现总金额 单位为：RMB Yuan。取值范围为[0.01，99999999.99]，精确到小数点后两位。
     */
    private String total_amount;

    /**
     * 会计日期：日账单格式为yyyyMMdd
     */
    private String account_date;

    /**
     * 交易流水
     */
    private String trade_no;

    /**
     * 参考手续费,单位为：RMB Yuan。取值范围为[0.01，99999999.99]，精确到小数点后两位。
     */
    private String fee;
}
