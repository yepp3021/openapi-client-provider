package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.BILL_DOWNLOAD, type = ApiMessageType.Response)
public class YinShengbillDownloadResponse extends YinShengResponse{
    /**
     * 下载url,
     * http://download.ysepay.com/Cv1dF1Cuy8ezofoFAAADB_ClX6I350.zip
     */
    private String 	bill_download_url;
}
