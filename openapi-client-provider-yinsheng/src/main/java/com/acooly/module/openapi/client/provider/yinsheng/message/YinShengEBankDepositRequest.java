package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.NETBANK_DEPOSIT, type = ApiMessageType.Request)
public class YinShengEBankDepositRequest extends YinShengRequest {

    /**
     * 店铺时间YYYYMMdd
     */
    private String shopdate;
    /**
     * 商户订单号
     */
    @NotBlank
    private String out_trade_no;
    /**
     * 通知地址
     */
    @NotBlank
    private String notify_url;

    /**
     * 页面跳转
     */
    @NotBlank
    private String return_url;

    /**
     * 订单总金额
     */
    @NotBlank
    private String total_amount;

    /**
     * 商品描述
     */
    private String subject;

    /**
     * 商户号
     */
    private String seller_id;

    /**
     * 商户名
     */
    private String seller_name;

    /**
     * 未付款时间
     */
    private String timeout_express = "3d";

    /**
     * 支付模式
     */
    private String pay_mode = "internetbank";

    /**
     * 银行类型
     */
    private String bank_type;

    /**
     * 银行卡类型
     */
    private String bank_account_type;

    /**
     * 银行卡类型
     */
    private String support_card_type;

    /**
     * 业务代码
     */
    private String business_code;
}
