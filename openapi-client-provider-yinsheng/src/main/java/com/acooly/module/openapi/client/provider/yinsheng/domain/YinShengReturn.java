/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.yinsheng.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 * 
 */
@Getter
@Setter
public class YinShengReturn extends YinShengNotify {

}
