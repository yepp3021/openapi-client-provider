/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhike 2017-09-24 19:29
 */
public enum ShengpayServiceNameEnum implements Messageable {
  UNKNOWN("UNKNOWN", "未知", ShengpayApiServiceType.SYNC),
  PRECHECK_FOR_SIGN("precheckForSign", "签约预校验", ShengpayApiServiceType.SYNC),
  SIGN_CONFIRM("sign", "签约确认", ShengpayApiServiceType.SYNC),
  CREATE_PAYMENT_ORDER("createPaymentOrder", "创建支付订单", ShengpayApiServiceType.SYNC),
  PRECHECK_FOR_PAYMENT("precheckForPayment", "支付预校验", ShengpayApiServiceType.SYNC),
  PAYMENT_CONFIRM("payment", "确认支付", ShengpayApiServiceType.ASYNC),
  UNSIGN("unsign", "解约", ShengpayApiServiceType.SYNC),
  QUERY_AGREEMENT("queryAgreement", "协议查询", ShengpayApiServiceType.SYNC),
  ORDER_QUERY("queryOrderService", "单笔订单查询", ShengpayApiServiceType.SYNC),
  ;

  private final String code;
  private final String message;
  private final ShengpayApiServiceType apiServiceType;

  private ShengpayServiceNameEnum(
      String code, String message, ShengpayApiServiceType apiServiceType) {
    this.code = code;
    this.message = message;
    this.apiServiceType = apiServiceType;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  @Override
  public String code() {
    return code;
  }

  @Override
  public String message() {
    return message;
  }

  public ShengpayApiServiceType getApiServiceType() {
    return apiServiceType;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = Maps.newLinkedHashMap();
    for (ShengpayServiceNameEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static ShengpayServiceNameEnum find(String code) {
    for (ShengpayServiceNameEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<ShengpayServiceNameEnum> getAll() {
    List<ShengpayServiceNameEnum> list = new ArrayList<ShengpayServiceNameEnum>();
    for (ShengpayServiceNameEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (ShengpayServiceNameEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
