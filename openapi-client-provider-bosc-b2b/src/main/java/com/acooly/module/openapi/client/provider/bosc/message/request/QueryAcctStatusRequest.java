package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

@Getter
@Setter
public class QueryAcctStatusRequest extends BoscRequestDomain {

	/**
	 * 对公电子账户号
	 */
	@NotBlank
	private String eAcctNo;

}
