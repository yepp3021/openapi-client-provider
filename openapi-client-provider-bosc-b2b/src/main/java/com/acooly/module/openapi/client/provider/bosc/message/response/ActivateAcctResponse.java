package com.acooly.module.openapi.client.provider.bosc.message.response;

import lombok.Getter;
import lombok.Setter;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;

@Getter
@Setter
public class ActivateAcctResponse extends BoscResponseDomain {

	/**
	 * 打款激活金额
	 */
	private Money amount;

	/**
	 * 打款激活最后日期
	 */
	private String actiDeadline;

}
