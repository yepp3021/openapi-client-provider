/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.fudian.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.fudian.FudianConstants;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步请求报文组装
 *
 * @author zhangpu 2018-1-23 16:11
 */
@Slf4j
@Component
public class FudianRequestMarshall extends FudianAbstractMarshall implements ApiMarshal<String, FudianRequest> {

    @Override
    public String marshal(FudianRequest source) {
        StringBuilder sb = new StringBuilder();
        sb.append(FudianConstants.REQUEST_PARAM_NAME)
                .append("=")
                .append(doMarshall(source));
        log.info("请求报文: {}", sb.toString());
        return sb.toString();
    }


}
