/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.fudian;

import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.message.fund.*;
import com.acooly.module.openapi.client.provider.fudian.message.member.*;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataRequest;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataResponse;
import com.acooly.module.openapi.client.provider.fudian.message.other.QueryDownloadLogFilesRequest;
import com.acooly.module.openapi.client.provider.fudian.message.other.QueryDownloadLogFilesResponse;
import com.acooly.module.openapi.client.provider.fudian.message.query.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Component
public class FudianApiService {

    @Resource(name = "fudianApiServiceClient")
    private FudianApiServiceClient fudianApiServiceClient;

    /**
     * 个人用户注册(跳转)
     *
     * @param request
     * @return
     */
    public String userRegister(UserRegisterRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 企业开户
     */
    public String corpRegister(CorpRegisterRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * PC跳转充值
     *
     * @return
     */
    public String accountRecharge(AccountRechargeRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * app跳转充值
     *
     * @return
     */
    public String appRealPayRecharge(AppRealPayRechargeRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 提现申请
     *
     * @param request
     * @return
     */
    public String accountWithdraw(AccountWithdrawRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 投资人回款
     *
     * @param request
     * @return
     */
    public LoanCallbackResponse loanCallback(LoanCallbackRequest request) {
        return (LoanCallbackResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 借款人自动免密还款
     *
     * @return
     */
    public LoanFastRepayResponse loanFastRepay(LoanFastRepayRequest request) {
        return (LoanFastRepayResponse)fudianApiServiceClient.execute(request);
    }

    /**
     * 流标
     *
     * @return
     */
    public LoanCancelResponse loanCancel(LoanCancelRequest request) {
        return (LoanCancelResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 发标
     *
     * @param request
     * @return
     */
    public LoanCreateResponse loanCreate(LoanCreateRequest request) {
        return (LoanCreateResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 自动投标
     * @param request
     * @return
     */
    public LoanFastInvestResponse loanFastInvest(LoanFastInvestRequest request) {
        return (LoanFastInvestResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 债权认购
     * @param request
     * @return
     */
    public String loancreditInvest(LoancreditInvestRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 商户转壮（奖励）
     * @param request
     * @return
     */
    public MerchantTransferResponse merchantTransfer(MerchantTransferRequest request) {
        return (MerchantTransferResponse) fudianApiServiceClient.execute(request);
    }
    /**
     * 满标
     * @param request
     * @return
     */
    public LoanFullResponse loanFull(LoanFullRequest request) {
        return (LoanFullResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 手动投标
     * @param request
     * @return
     */
    public String loanInvest(LoanInvestRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 手动还款
     * @param request
     * @return
     */
    public String loanRepay(LoanRepayRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 授权和取消授权
     * @param request
     * @return
     */
    public String businessAuthorization(BusinessAuthorizationRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 企业信息变更
     * @param request
     * @return
     */
    public String corpModify(CorpModifyRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 修改手机号码
     * @param request
     * @return
     */
    public String phoneUpdate(PhoneUpdateRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 重置交易密码
     * @param request
     * @return
     */
    public String pwdReset(PwdResetRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 绑定银行卡
     * @param request
     * @return
     */
    public String userCardBind(UserCardBindRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 解绑银行卡(不需要审核)
     * @param request
     * @return
     */
    public String userCardCancelBind(UserCardCancelBindRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 解绑银行卡（需要审核）
     * @param request
     * @return
     */
    public String userCardAutitCancelBind(UserCardAuditCancelBindRequest request) {
        return fudianApiServiceClient.redirectGet(request);
    }

    /**
     * 数据迁移
     *
     * @param request
     * @return
     */
    public MigrationDataResponse migrationData(MigrationDataRequest request) {
        return (MigrationDataResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 对账文件下载
     *
     * @param request
     * @return
     */
    public QueryDownloadLogFilesResponse queryDownloadLogFiles(QueryDownloadLogFilesRequest request) {
        return (QueryDownloadLogFilesResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 标的查询
     *
     * @param request
     * @return
     */
    public QueryLoanResponse queryLoan(QueryLoanRequest request) {
        return (QueryLoanResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 账户流水查询
     *
     * @param request
     * @return
     */
    public QueryLogAccountResponse queryLogAccount(QueryLogAccountRequest request) {
        return (QueryLogAccountResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 标的流水查询
     *
     * @param request
     * @return
     */
    public QueryLogLoanAccountResponse queryLogLoanAccount(QueryLogLoanAccountRequest request) {
        return (QueryLogLoanAccountResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 提现退汇查询
     *
     * @param request
     * @return
     */
    public QueryRetremitResponse queryRetremit(QueryRetremitRequest request) {
        return (QueryRetremitResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 订单查询
     *
     * @param request
     * @return
     */
    public QueryTradeResponse queryTrade(QueryTradeRequest request) {
        return (QueryTradeResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 用户查询
     *
     * @param request
     * @return
     */
    public QueryUserResponse queryUser(QueryUserRequest request) {
        return (QueryUserResponse) fudianApiServiceClient.execute(request);
    }

    /**
     * 同步，异步通知转化
     * serviceKey :FudianServiceNameEnum枚举中取对应的code
     * @param request 请求
     * @param serviceKey 服务标识
     * @return
     */
    public FudianNotify notice(HttpServletRequest request, String serviceKey) {
        return  fudianApiServiceClient.notice(request, serviceKey);
    }
}
