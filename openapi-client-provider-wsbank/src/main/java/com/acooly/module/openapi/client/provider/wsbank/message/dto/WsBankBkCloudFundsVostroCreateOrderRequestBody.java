package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsBankBkCloudFundsVostroCreateOrderRequestBody implements Serializable {

	private static final long serialVersionUID = -9197091061378131081L;

	/**
	 * 合作方机构号
	 */
	@Size(max = 32)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;
	
	/**
	 * 商户号
	 */
	@Size(max = 32)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;
	
	/**
	 * 收款方Id
	 */
	@Size(max = 32)
	@XStreamAlias("PayeeId")
	@NotBlank
	private String payeeId;
	
	/**
	 * 收款方类型
	 */
	@Size(max = 16)
	@XStreamAlias("PayeeType")
	@NotBlank
	private String payeeType;
	
	/**
	 * 外部请求流水号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;
	
	/**
	 * 付款方id(平台id)
	 */
	@Size(max = 64)
	@XStreamAlias("PayerUserId")
	private String payerUserId;
	
	/**
	 * 付款方姓名
	 */
	@Size(max = 128)
	@XStreamAlias("PayerName")
	private String payerName;
	
	/**
	 * 付款方卡号
	 */
	@Size(max = 64)
	@XStreamAlias("PayerCardNo")
	private String payerCardNo;
	
	/**
	 * 订单金额(金额为分)
	 */
	@XStreamAlias("TotalAmount")
	@NotBlank
	private String totalAmount;
	
	/**
	 * 币种，默认CNY
	 */
	@Size(max = 3)
	@XStreamAlias("Currency")
	@NotBlank
	private String currency = "CNY";
	
	/**
	 * 商品描述
	 */
	@Size(max = 256)
	@XStreamAlias("Body")
	@NotBlank
	private String body;
	
	/**
	 * 商品标记
	 */
	@Size(max = 64)
	@XStreamAlias("GoodsTag")
	private String goodsTag;
	
	/**
	 * 商品详情列表。JSON格式，会透传至第三方支付
	 */
	@Size(max = 256)
	@XStreamAlias("GoodsDetail")
	private String goodsDetail;
	
	/**
	 * 备注
	 */
	@Size(max = 256)
	@XStreamAlias("Memo")
	private String memo;
}
