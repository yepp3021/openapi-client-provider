package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsBankBkCloudFundsVostroCreateOrderRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.VOSTRO_CREATE_ORDER,type = ApiMessageType.Request)
public class WsBankBkCloudFundsVostroCreateOrderRequest extends WsbankRequest {

    @XStreamAlias("request")
    private WsBankBkCloudFundsVostroCreateOrderRequestInfo requestInfo;
}
