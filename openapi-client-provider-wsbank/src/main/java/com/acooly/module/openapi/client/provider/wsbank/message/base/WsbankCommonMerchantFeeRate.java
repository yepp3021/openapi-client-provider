package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsbankCommonMerchantFeeRate implements Serializable {

	private static final long serialVersionUID = 6349060248047361507L;

	/**
	 * 渠道类型。可选值： 01：支付宝 02：微信支付
	 */
	@NotBlank
	@JSONField(name="ChannelType")
	@Size(max =8)
	private String channelType;

	/**
	 * 费用类型。可选值： 01：t0收单手续费 02：t1收单手续费
	 */
	@NotBlank
	@Size(max = 8)
	@JSONField(name="FeeType")
	private String feeType;

	/**
	 * 费率
	 */
	@NotBlank
	@Size(max = 15)
	@JSONField(name="FeeValue")
	private String feeValue;

	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
