package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhike 2018/7/10 16:31
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CHANGE_BANK_MOBILE_ADVANCE, type = ApiMessageType.Response)
public class ChangeBankMobileAdvanceResponse extends SinapayResponse {

    /**
     *修改后的银行卡Id
     */
    @NotEmpty
    @ApiItem(value = "card_id")
    private String cardId;

    /**
     *银行卡是否已认证
     * Y：已认证；N：未认证
     */
    @ApiItem(value = "is_verified")
    private String isVerified;
}
