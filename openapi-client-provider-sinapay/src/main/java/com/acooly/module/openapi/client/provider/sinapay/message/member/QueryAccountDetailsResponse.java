/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.member.dto.SinapayTradeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 
 * 交易订单查询 响应报文
 * 
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_ACCOUNT_DETAILS, type = ApiMessageType.Response)
public class QueryAccountDetailsResponse extends SinapayResponse {

	/**
	 * 收支明细列表
	 * 详见“收支条目参数”条目按时间倒序排列，每个条目中的参数用“^”分隔，条目与条
	 */
	@ApiItem("detail_list")
	private List<SinapayTradeInfo> tradeList;

	/**
	 * 页号
	 *
	 * 页号，从1开始，默认为1
	 */
	@Min(1)
	@ApiItem(value = "page_no")
	private int pageNo;

	/**
	 * 每页大小
	 *
	 * 每页记录数，默认20
	 */
	@Min(1)
	@ApiItem(value = "page_size")
	private int pageSize;

	/**
	 * 总计录数
	 */
	@Size(max = 10)
	@ApiItem(value = "total_item")
	private int totalItem;

	/**
	 * 总收入
	 * 单位为：RMB Yuan。精确到小数点后两位。
	 */
	@ApiItem(value = "total_income")
	private Money totalIncome;

	/**
	 *总支出
	 * 单位为：RMB Yuan。精确到小数点后两位。
	 */
	@MoneyConstraint
	@ApiItem(value = "total_payout")
	private Money totalPayout;


	/**
	 * 总收益
	 * 单位为：RMB Yuan。精确到小数点后两位。该参数属于1.1版本，即“version”中设置为1.1时，才会返回该参数。
	 */
	@ApiItem(value = "total_bonus")
	private Money totalBonus;
}
