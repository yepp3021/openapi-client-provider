package com.acooly.module.openapi.client.provider.sinapay.exception;

/**
 * @author zhike 2018/3/15 14:28
 */
public class SinapayProcessingException extends RuntimeException{

    public SinapayProcessingException() {
    }

    public SinapayProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public SinapayProcessingException(String message) {
        super(message);
    }

    public SinapayProcessingException(Throwable cause) {
        super(cause);
    }
}
