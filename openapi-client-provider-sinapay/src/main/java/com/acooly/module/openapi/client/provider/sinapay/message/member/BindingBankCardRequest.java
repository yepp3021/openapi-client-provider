/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.api.exception.ApiMessageCheckException;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.BINDING_BANK_CARD, type = ApiMessageType.Request)
public class BindingBankCardRequest extends SinapayRequest {

	public static final String CARD_TYPE_DEBIT = "DEBIT";
	public static final String CARD_TYPE_CREDIT = "CREDIT";

	public static final String CARD_ATTR_PRI = "C";
	public static final String CARD_ATTR_PUB = "B";

	/** 绑卡请求号 */
	@Size(max = 32)
	@NotEmpty
	@ApiItem("request_no")
	private String requestNo = Ids.oid();

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 银行编码
	 *com.acooly.module.openapi.client.provider.sinapay.enums.SinapayBankCode
	 */
	@Size(max = 10)
	@NotEmpty
	@ApiItem(value = "bank_code")
	private String bankCode;

	/** 银行卡号 */
	@Size(max = 30)
	@NotEmpty
	@ApiItem(value = "bank_account_no", securet = true)
	private String bankAccountNo;

	/** 银行户名 */
	@Size(max = 50)
	@ApiItem(value = "account_name", securet = true)
	private String accountName;

	/** 卡类型
	 *com.acooly.module.openapi.client.provider.sinapay.enums.SinapayCardType
	 */
	@Size(max = 10)
	@NotEmpty
	@ApiItem(value = "card_type")
	private String cardType = CARD_TYPE_DEBIT;

	/** 卡属性
	 *com.acooly.module.openapi.client.provider.sinapay.enums.SinapayCardAttribute
	 */
	@Size(max = 10)
	@NotEmpty
	@ApiItem(value = "card_attribute")
	private String cardAttribute = CARD_ATTR_PRI;

	/** 证件类型 可选:IC */
	@Size(max = 18)
	@ApiItem(value = "cert_type")
	private String certType = "IC";

	/** 证件号码 */
	@Size(max = 18)
	@ApiItem(value = "cert_no", securet = true)
	private String certNo;

	/** 银行预留手机号 如认证方式不为空，则要求此信息也不能为空 */
	@Size(max = 20)
	@ApiItem(value = "phone_no", securet = true)
	private String phoneNo;

	/** 信用卡有效期 信用卡专用，有效期(10/13)，（月份/年份） */
	@Size(max = 10)
	@ApiItem(value = "validity_period")
	private String validityPeriod;

	/** 信用卡CVV2 */
	@Size(max = 10)
	@ApiItem(value = "verification_value")
	private String verificationValue;

	/** 省 汉字 如：上海市，江苏省 */
	@Size(max = 128)
	@NotEmpty
	private String province;
	/** 市 汉字 如：上海市，南京市 */
	@Size(max = 128)
	@NotEmpty
	private String city;

	/** 开户支行名称 */
	@Size(max = 255)
	@ApiItem(value = "bank_branch")
	private String bankBranch;

	/** 认证方式 银行卡真实性认证方式，见附录“卡认证方式”，空则表示不认证。 可选值：SIGN
	 * com.acooly.module.openapi.client.provider.sinapay.enums.VerifyModeEnum
	 */
	@Size(max = 16)
	@ApiItem(value = "verify_mode")
	private String verifyMode;

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;

	@Override
	public void doCheck() throws ApiMessageCheckException {
		super.doCheck();
		if (Strings.isNotBlank(verifyMode) && Strings.isBlank(phoneNo)) {
			throw new ApiMessageCheckException("phoneNo", "verifyMode不为空时，phoneNo必填。");
		}
	}
}
