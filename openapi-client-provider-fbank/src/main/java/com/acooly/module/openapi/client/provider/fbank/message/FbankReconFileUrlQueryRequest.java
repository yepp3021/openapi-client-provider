package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 11:48
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_DOWNLOAD_FILE, type = ApiMessageType.Request)
public class FbankReconFileUrlQueryRequest extends FbankRequest {

    /**
     * 交易日期
     * 获取该天的结算数据；格式：yyyyMMdd
     */
    @Size(max = 19)
    private String orderDt;

    /**
     * 文件类型，只能为“1”
     */
    private String fileType = "1";

    @Override
    public String getService() {
        return FbankServiceEnum.TRADE_DOWNLOAD_FILE.getKey();
    }
}
