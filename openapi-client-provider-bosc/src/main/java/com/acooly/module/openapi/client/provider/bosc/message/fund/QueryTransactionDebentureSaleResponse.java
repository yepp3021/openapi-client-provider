package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.DebentureSaleDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_DEBENTURE_SALE ,type = ApiMessageType.Response)
public class QueryTransactionDebentureSaleResponse extends BoscResponse {
	
	private List<DebentureSaleDetailInfo> records;
	
	public List<DebentureSaleDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<DebentureSaleDetailInfo> records) {
		this.records = records;
	}
}