package com.acooly.module.openapi.client.provider.yipay.exception;

/**
 * @author zhike 2018/3/15 14:28
 */
public class YipayProcessingException extends RuntimeException{

    public YipayProcessingException() {
    }

    public YipayProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public YipayProcessingException(String message) {
        super(message);
    }

    public YipayProcessingException(Throwable cause) {
        super(cause);
    }
}
