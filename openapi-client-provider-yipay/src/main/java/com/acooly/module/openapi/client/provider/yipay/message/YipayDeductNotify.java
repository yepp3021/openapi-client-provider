package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayNotify;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 19:02
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.DEDUCT,type = ApiMessageType.Notify)
public class YipayDeductNotify extends YipayNotify {

    /**
     * 交易状态编码
     * 000:成功
     * 004:失败
     */
    private String statCode;

    /**
     * 交易结果编码
     */
    private String resultCode;

    /**
     * 交易结果描述
     */
    private String resultMsg;

    /**
     * 商户订单号
     * 交易发起交易流水标识，可用于对账
     */
    private String reqSeq;

    /**
     * 外部订单号
     * 交易时商户系统上送的外部订单号
     */
    private String extOrderSeq;

    /**
     * 翼支付订单号（对账单号）
     * 翼支付系统生成的订单号，用于对账
     */
    private String coreOrderCode;

    /**
     * 交易金额
     * 交易金额(不含手续费)，只能为小于18位的非零正整数，以分为单位，只支持人民币
     */
    private String tradeAmount;

    /**
     * 请求发起时间
     * 请求发起时间，毫秒数
     */
    private String  reqTime;

    /**
     * 交易完成时间
     * 交易完成时间，毫秒数
     */
    private String tradeFinTime;

    /**
     * 平台管理商户号
     * 发起交易的平台管理商户号
     */
    private String merchantNo;

}
