package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.CERT_TWO_ELEMENTS_VERIFY,type = ApiMessageType.Request)
public class YipayCertTwoElementsVerifyRequest extends YipayRequest {


    /**
     * 证件号码
     */
    @NotBlank
    @Size(max = 64)
    @YipayAlias(value = "certNo")
    private String certNo;

    /**
     * 证件类型
     * 00 身份证
     * 01 护照
     * 02 军人证
     * 03 户口簿
     * 04 武警证
     * 05 法人营业执照
     * 06 港澳通行证
     * 07 台湾通行证
     * 08 学生证
     * 09 工作证
     * 10 工商执照
     * 11 警官证
     * 12 事业单位编码
     * 13 房产证
     * 51 组织机构编码
     * 99 其它证件
     */
    @NotBlank
    @Size(max = 2)
    @YipayAlias(value = "certType")
    private String certType = "00";

    /**
     * 姓名
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "name")
    private String realName;
}
