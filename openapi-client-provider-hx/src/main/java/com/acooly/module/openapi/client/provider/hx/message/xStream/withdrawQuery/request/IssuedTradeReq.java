package com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.request;

import com.acooly.module.openapi.client.provider.hx.message.xStream.common.ReqHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 9:52.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("IssuedTradeReq")
public class IssuedTradeReq {

    @XStreamAlias("head")
    private ReqHead head;

    @XStreamAlias("body")
    private ReqWithdrawQueryBody body;

}
