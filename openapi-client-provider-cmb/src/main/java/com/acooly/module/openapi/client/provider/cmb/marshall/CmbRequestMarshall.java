/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.cmb.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class CmbRequestMarshall extends CmbMarshallSupport implements ApiMarshal<String, CmbRequest> {

    @Override
    public String marshal(CmbRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
