/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.cmb;

import com.acooly.module.openapi.client.provider.cmb.message.HthDirectPayRequest;
import com.acooly.module.openapi.client.provider.cmb.message.HthDirectPayResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class CmbApiService {
	
	@Resource(name = "cmbApiServiceClient")
	private CmbApiServiceClient cmbApiServiceClient;

	@Autowired
	private CmbProperties cmbProperties;

	/**
	 * 快捷支付
	 * @param request
	 * @return
	 */
	public HthDirectPayResponse hthDirectPayService(HthDirectPayRequest request) {
		return (HthDirectPayResponse) cmbApiServiceClient.execute(request);
	}
}
