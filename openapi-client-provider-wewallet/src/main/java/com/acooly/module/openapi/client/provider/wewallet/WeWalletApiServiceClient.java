/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.wewallet;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletNotify;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletResponse;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletRedirectMarshall;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletRequestMarshall;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletResponseUnmarshall;
import com.acooly.module.openapi.client.provider.wewallet.marshall.WeWalletSignMarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("weWalletApiServiceClient")
public class WeWalletApiServiceClient
        extends AbstractApiServiceClient<WeWalletRequest, WeWalletResponse, WeWalletNotify, WeWalletNotify> {

    public static final String PROVIDER_NAME = "webank";

    @Resource(name = "weWalletHttpTransport")
    private Transport transport;
    @Resource(name = "weWalletRequestMarshall")
    private WeWalletRequestMarshall requestMarshal;
    @Resource(name = "weWalletRedirectMarshall")
    private WeWalletRedirectMarshall redirectMarshal;
    @Resource(name = "weWalletRedirectPostMarshall")
    private WeWalletRedirectPostMarshall weWalletRedirectPostMarshall;
    @Resource(name = "weWalletSignMarshall")
    private WeWalletSignMarshall signMarshall;

    @Autowired
    private WeWalletResponseUnmarshall responseUnmarshal;

    @Autowired
    private WeWalletNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private WeWalletProperties weWalletProperties;

    @Override
    public WeWalletResponse execute(WeWalletRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(weWalletProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().exchange(requestMessage, requestMessage);
            WeWalletResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public WeWalletNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String,String> notifyData = getSignMarshall().getDateMap(request);
            WeWalletNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public WeWalletSignMarshall getSignMarshall() {
        return signMarshall;
    }

    @Override
    public PostRedirect redirectPost(WeWalletRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = weWalletRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, WeWalletRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<WeWalletResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<WeWalletNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, WeWalletRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<WeWalletNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }


}
