/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.wewallet.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.wewallet.WeWalletConstants;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletNotify;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;
import com.acooly.module.openapi.client.provider.wewallet.partner.WeWalletKeyPairManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

/**
 * @author zhangpu
 */
@Service
public class WeWalletNotifyUnmarshall extends WeWalletMarshallSupport
        implements ApiUnmarshal<WeWalletNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(WeWalletNotifyUnmarshall.class);

    @Resource(name = "weWalletMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "weWalletGetKeyPairManager")
    private WeWalletKeyPairManager keyPairManager;

    @SuppressWarnings("unchecked")
    @Override
    public WeWalletNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);
            String signature = message.get(WeWalletConstants.SIGN);
            message.remove(WeWalletConstants.SIGN);
           // String plain = SignUtils.getSignContent(message);
            WeWalletNotify weWalletNotify = doUnmarshall(message, serviceName);
            //验签
            //Safes.getSigner(SignTypeEnum.Rsa.name ()).verify(plain, keyPairManager.getKeyPair(weWalletNotify.getPartnerId()), signature);
            return weWalletNotify;
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }
    }

    protected WeWalletNotify doUnmarshall(Map<String, String> message, String serviceName) {
        WeWalletNotify notify = (WeWalletNotify) messageFactory.getNotify(serviceName);
        Set<Field> fields = Reflections.getFields(notify.getClass());
        String key = null;
        for (Field field : fields) {
            ApiItem apiItem = field.getAnnotation(ApiItem.class);
            if (apiItem != null && Strings.isNotBlank(apiItem.value())) {
                key = apiItem.value();
            }else {
                key = field.getName();
            }
            if(Strings.equals("merId",key)){
                notify.setPartnerId(message.get(key));
            }
            Reflections.invokeSetter(notify, field.getName(), message.get(key));
        }
        notify.setBizType(WeWalletServiceEnum.find(serviceName).getCode());
        //if (Strings.isNotBlank(notify.getRespCode())) {
        //    notify.setRespMsg(WeWalletRespCodes.getMessage(notify.getRespCode()));
        //}
        return notify;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

}
