/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum AllinpayBankCodeEnum implements Messageable {

    BANK_CODE_0102("0102", "中国工商银行"),
    BANK_CODE_0103("0103", "中国农业银行"),
    BANK_CODE_0104("0104", "中国银行"),
    BANK_CODE_0105("0105", "中国建设银行"),
    BANK_CODE_0201("0201", "国家开发银行"),
    BANK_CODE_0202("0202", "中国进出口银行"),
    BANK_CODE_0203("0203", "中国农业发展银行"),
    BANK_CODE_0301("0301", "交通银行"),
    BANK_CODE_0302("0302", "中信银行"),
    BANK_CODE_0303("0303", "中国光大银行"),
    BANK_CODE_0304("0304", "华夏银行"),
    BANK_CODE_0305("0305", "中国民生银行"),
    BANK_CODE_0306("0306", "广东发展银行"),
    BANK_CODE_0307("0307", "平安银行"),
    BANK_CODE_0308("0308", "招商银行"),
    BANK_CODE_0309("0309", "兴业银行"),
    BANK_CODE_0310("0310", "上海浦东发展银行"),
    BANK_CODE_0313("0313", "城市商业银行"),
    BANK_CODE_03131610("03131610", "晋商银行"),
    BANK_CODE_03132210("03132210", "盛京银行"),
    BANK_CODE_03133010("03133010", "江苏银行"),
    BANK_CODE_03133310("03133310", "杭州银行"),
    BANK_CODE_03133450("03133450", "浙江泰隆商业银行"),
    BANK_CODE_03134510("03134510", "齐鲁银行"),
    BANK_CODE_03134530("03134530", "齐商银行"),
    BANK_CODE_03134580("03134580", "潍坊银行"),
    BANK_CODE_03134680("03134680", "德州银行"),
    BANK_CODE_03135210("03135210", "汉口银行"),
    BANK_CODE_03135220("03135220", "黄石银行"),
    BANK_CODE_03136110("03136110", "广西北部湾银行"),
    BANK_CODE_03136620("03136620", "遂宁银行"),
    BANK_CODE_03136730("03136730", "南充银行"),
    BANK_CODE_03143020("03143020", "江苏锡州农村商业银行"),
    BANK_CODE_03143040("03143040", "江苏江南农村商业银行"),
    BANK_CODE_03143055("03143055", "江苏常熟农村商业银行"),
    BANK_CODE_0315("0315", "恒丰银行"),
    BANK_CODE_0316("0316", "浙商银行"),
    BANK_CODE_0317("0317", "农村合作银行"),
    BANK_CODE_0318("0318", "渤海银行"),
    BANK_CODE_0319("0319", "徽商银行"),
    BANK_CODE_0320("0320", "镇银行有限责任公司"),
    BANK_CODE_0401("0401", "城市信用社"),
    BANK_CODE_04012900("04012900", "上海银行"),
    BANK_CODE_0402("0402", "农信银"),
    BANK_CODE_04021000("04021000", "北京农商行"),
    BANK_CODE_04021770("04021770", "尧都农信社"),
    BANK_CODE_04023010("04023010", "南京市区农村信用社"),
    BANK_CODE_04023310("04023310", "杭州联合银行"),
    BANK_CODE_04023320("04023320", "宁波市农村合作信用联社"),
    BANK_CODE_04023610("04023610", "合肥科技农村商业银行"),
    BANK_CODE_04023620("04023620", "芜湖扬子农村商业银行"),
    BANK_CODE_04024210("04024210", "江西农信社"),
    BANK_CODE_04024211("04024211", "南昌洪都农村商业银行营业部"),
    BANK_CODE_04024560("04024560", "烟台市芝罘区农村信用社"),
    BANK_CODE_04024750("04024750", "菏泽市牡丹区城区农村信用社"),
    BANK_CODE_04025210("04025210", "武汉农村商业银行"),
    BANK_CODE_04025840("04025840", "深圳农村商业银行"),
    BANK_CODE_04025880("04025880", "顺德农村商业银行"),
    BANK_CODE_04026530("04026530", "重庆农商行"),
    BANK_CODE_0403("0403", "中国邮政储蓄银行"),
    BANK_CODE_04031000("04031000", "北京银行"),
    BANK_CODE_04035810("04035810", "广东邮政储蓄银行"),
    BANK_CODE_04053910("04053910", "福州银行"),
    BANK_CODE_04083320("04083320", "宁波银行"),
    BANK_CODE_04123330("04123330", "温州银行"),
    BANK_CODE_04135810("04135810", "广州银行"),
    BANK_CODE_04202220("04202220", "大连银行"),
    BANK_CODE_04243010("04243010", "南京银行"),
    BANK_CODE_04256020("04256020", "东莞银行"),
    BANK_CODE_04296510("04296510", "成都银行"),
    BANK_CODE_04422610("04422610", "哈尔滨银行"),
    BANK_CODE_04447910("04447910", "西安银行"),
    BANK_CODE_04484220("04484220", "南昌银行"),
    BANK_CODE_04504520("04504520", "青岛银行"),
    BANK_CODE_04554732("04554732", "日照银行"),
    BANK_CODE_04615510("04615510", "长沙银行"),
    BANK_CODE_04643970("04643970", "泉州银行"),
    BANK_CODE_04652280("04652280", "营口银行"),
    BANK_CODE_04814650("04814650", "威海商业银行"),
    BANK_CODE_0501("0501", "汇丰银行"),
    BANK_CODE_0502("0502", "东亚银行"),
    BANK_CODE_0503("0503", "南洋商业银行"),
    BANK_CODE_0504("0504", "恒生银行(中国)有限公司"),
    BANK_CODE_0505("0505", "中国银行(香港)有限公司"),
    BANK_CODE_0506("0506", "集友银行有限公司"),
    BANK_CODE_0507("0507", "创兴银行有限公司"),
    BANK_CODE_0509("0509", "星展银行(中国)有限公司"),
    BANK_CODE_0510("0510", "永亨银行(中国)有限公司"),
    BANK_CODE_0512("0512", "永隆银行"),
    BANK_CODE_0531("0531", "花旗银行(中国)有限公司"),
    BANK_CODE_0532("0532", "美国银行有限公司"),
    BANK_CODE_0533("0533", "摩根大通银行(中国)有限公司"),
    BANK_CODE_0561("0561", "三菱东京日联银行(中国)有限公司"),
    BANK_CODE_0563("0563", "日本三井住友银行股份有限公司"),
    BANK_CODE_0564("0564", "瑞穗实业银行(中国)有限公司"),
    BANK_CODE_0565("0565", "日本山口银行股份有限公司"),
    BANK_CODE_0591("0591", "韩国外换银行股份有限公司"),
    BANK_CODE_0593("0593", "友利银行(中国)有限公司"),
    BANK_CODE_0594("0594", "韩国产业银行"),
    BANK_CODE_0595("0595", "新韩银行(中国)有限公司"),
    BANK_CODE_0596("0596", "韩国中小企业银行有限公司"),
    BANK_CODE_0597("0597", "韩亚银行(中国)有限公司"),
    BANK_CODE_0621("0621", "华侨银行(中国)有限公司"),
    BANK_CODE_0622("0622", "大华银行(中国)有限公司"),
    BANK_CODE_0623("0623", "星展银行(中国)有限公司"),
    BANK_CODE_0631("0631", "泰国盘谷银行(大众有限公司)"),
    BANK_CODE_0641("0641", "奥地利中央合作银行股份有限公司"),
    BANK_CODE_0651("0651", "比利时联合银行股份有限公司"),
    BANK_CODE_0652("0652", "比利时富通银行有限公司"),
    BANK_CODE_0661("0661", "荷兰银行"),
    BANK_CODE_0662("0662", "荷兰安智银行股份有限公司"),
    BANK_CODE_0671("0671", "渣打银行"),
    BANK_CODE_0672("0672", "英国苏格兰皇家银行公众有限公司"),
    BANK_CODE_0691("0691", "法国兴业银行(中国)有限公司"),
    BANK_CODE_0694("0694", "法国东方汇理银行股份有限公司"),
    BANK_CODE_0695("0695", "法国外贸银行股份有限公司"),
    BANK_CODE_0711("0711", "德国德累斯登银行股份公司"),
    BANK_CODE_0712("0712", "德意志银行(中国)有限公司"),
    BANK_CODE_0713("0713", "德国商业银行股份有限公司"),
    BANK_CODE_0714("0714", "德国西德银行股份有限公司"),
    BANK_CODE_0715("0715", "德国巴伐利亚州银行"),
    BANK_CODE_0716("0716", "德国北德意志州银行"),
    BANK_CODE_0732("0732", "意大利联合圣保罗银行股份有限公司"),
    BANK_CODE_0741("0741", "瑞士信贷银行股份有限公司"),
    BANK_CODE_0742("0742", "瑞士银行"),
    BANK_CODE_0751("0751", "加拿大丰业银行有限公司"),
    BANK_CODE_0752("0752", "加拿大蒙特利尔银行有限公司"),
    BANK_CODE_0761("0761", "澳大利亚和新西兰银行集团有限公司"),
    BANK_CODE_0771("0771", "摩根士丹利国际银行(中国)有限公司"),
    BANK_CODE_0775("0775", "联合银行(中国)有限公司"),
    BANK_CODE_0776("0776", "荷兰合作银行有限公司"),
    BANK_CODE_0781("0781", "厦门国际银行"),
    BANK_CODE_0782("0782", "法国巴黎银行(中国)有限公司"),
    BANK_CODE_0785("0785", "华商银行"),
    BANK_CODE_0787("0787", "华一银行"),
    BANK_CODE_0969("0969", "(澳门地区)银行"),
    BANK_CODE_0989("0989", "(香港地区)银行"),
    BANK_CODE_14055810("14055810", "广州市农村信用社"),
    BANK_CODE_14075882("14075882", "南海市农村信用社"),
    BANK_CODE_14105210("14105210", "武汉农商银行"),
    BANK_CODE_14156020("14156020", "东莞农商银行"),
    BANK_CODE_40258801("40258801", "佛山市禅城区农村信用社"),
    ;
    private final String code;
    private final String message;

    private AllinpayBankCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (AllinpayBankCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static AllinpayBankCodeEnum find(String code) {
        for (AllinpayBankCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<AllinpayBankCodeEnum> getAll() {
        List<AllinpayBankCodeEnum> list = new ArrayList<AllinpayBankCodeEnum>();
        for (AllinpayBankCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (AllinpayBankCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
