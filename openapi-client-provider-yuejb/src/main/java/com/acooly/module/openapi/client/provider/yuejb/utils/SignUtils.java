package com.acooly.module.openapi.client.provider.yuejb.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
public class SignUtils {
    /**
     * 生成待签字符串
     *
     * @param data 原始map数据
     * @return 待签字符串
     */
    public static String buildWaitingForSign(Map<String, String> data) {
        if (data == null || data.size() == 0) {
            throw new IllegalArgumentException("请求数据不能为空");
        }
        Map<String, String> sortedMap = new TreeMap<>(data);
        // 如果sign参数存在,去除sign参数,不参与签名
        if (sortedMap.containsKey("sign")) {
            sortedMap.remove("sign");
        }
        StringBuilder stringToSign = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            if (entry.getValue() != null) {
                stringToSign.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        stringToSign.deleteCharAt(stringToSign.length() - 1);
        return stringToSign.toString();
    }
    public static String buildWaitingForSignEncode(Map<String, String> data) {
        if (data == null || data.size() == 0) {
            throw new IllegalArgumentException("请求数据不能为空");
        }
        Map<String, String> sortedMap = new TreeMap<>(data);
        // 如果sign参数存在,去除sign参数,不参与签名
        if (sortedMap.containsKey("sign")) {
            sortedMap.remove("sign");
        }
        StringBuilder stringToSign = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            if (entry.getValue() != null) {
                try {
                    stringToSign.append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(),"UTF-8")).append("&");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        stringToSign.deleteCharAt(stringToSign.length() - 1);
        return stringToSign.toString();
    }
    public static String buildWaitingForSign(Map<String, String> data,boolean isEncoding) {
        if(isEncoding){
           return buildWaitingForSignEncode(data);
        }else {
           return buildWaitingForSign(data);
        }
    }
    /**
     * Map转换字符串
     * @param data
     * @return
     */
    public static String buildMapToString(Map<String, String> data) {
        if (data == null || data.size() == 0) {
            throw new IllegalArgumentException("请求数据不能为空");
        }
        Map<String, String> sortedMap = new TreeMap<>(data);
        StringBuilder stringToSign = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            if (entry.getValue() != null) {
                try {
                    stringToSign.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                } catch (Exception e) {
                    throw  new RuntimeException(e);
                }
            }
        }
        stringToSign.deleteCharAt(stringToSign.length() - 1);
        return stringToSign.toString();
    }
    /**
     * 验证MD5签名
     *
     * @param waitToSignStr 待签字符串
     * @param key           商户安全码
     * @param verifySign    待验证签名
     * @return 验签结果。 true: 成功, false: 失败
     */
    public static boolean verifyMD5(String waitToSignStr, String key, String verifySign) {
        // MD5摘要签名计算
        String signature = DigestUtils.md5Hex(waitToSignStr + key);
        return verifySign.equals(signature);
    }

    /**
     * 验证MD5签名
     *
     * @param data       报文数据
     * @param key        商户安全码
     * @param verifySign 待验证签名
     * @return 验签结果。 true: 成功, false: 失败
     */
    public static boolean verifyMD5(Map<String, String> data, String key, String verifySign) {
        // 生成待签字符串
        String waitToSignStr = buildWaitingForSign(data);
        // MD5摘要签名计算
        String signature = DigestUtils.md5Hex(waitToSignStr + key);
        return verifySign.equals(signature);
    }

}
