package com.acooly.module.openapi.client.provider.yuejb.domain;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBApiMessage implements ApiMessage {
    /**
     * 服务名称
     */
    @NotEmpty
    private String service;
    /**
     * 	商户ID
     */
    @NotEmpty
    private String partnerId;

    @Override
    public String getService() {
        return service;
    }
    @Override
    public String getPartner() {
        return partnerId;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setPartner(String partnerId) {
        this.partnerId = partnerId;
    }
}
