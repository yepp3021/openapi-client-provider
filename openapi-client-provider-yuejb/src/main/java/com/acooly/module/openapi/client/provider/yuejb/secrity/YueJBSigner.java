package com.acooly.module.openapi.client.provider.yuejb.secrity;

import com.acooly.module.openapi.client.provider.yuejb.YueJBConstants;
import com.acooly.module.safety.signature.Signer;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
@Component
public class YueJBSigner implements Signer<String, String> {
    @Override
    public String sign(String waitToSignStr, String key) {
        // MD5摘要签名计算
        try {
            byte[] bytes = (waitToSignStr + key).getBytes("UTF-8");
            return DigestUtils.md5Hex(bytes);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void verify(String waitToSignStr, String key, String verifySign) {
        // MD5摘要签名计算
        String signature = DigestUtils.md5Hex(waitToSignStr + key);
        if (!verifySign.equals(signature)) {
            throw new RuntimeException("签名未通过");
        }
    }

    @Override
    public String getSinType() {
        return YueJBConstants.SIGN_TYPE;
    }
}
