package com.acooly.module.openapi.client.provider.yibao.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_RECORD,type = ApiMessageType.Response)
public class YibaoRechargeOrderQueryResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 订单状态
     * ACCEPT：已接收
     * TO_VALIDATE：待短验确认
     * PAY_FAIL：支付失败
     * PROCESSING：处理中
     */
    @YibaoAlias(value = "status")
    private String status;


}
