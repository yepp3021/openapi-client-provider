/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.yibao.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum YibaoServiceNameEnum implements Messageable {

    UNKNOWN("UNKNOWN", "未知", YibaoApiServiceType.SYNC),
    YIBAO_BINDCARD_REQUEST("/bindcard/request", "有短验绑卡请求", YibaoApiServiceType.SYNC),
    YIBAO_BINDCARD_CONFIRM("/bindcard/confirm", "有短验绑卡请求短验确认", YibaoApiServiceType.SYNC),
    YIBAO_BINDCARD_RESENDSMS("/bindcard/resendsms", "有短验绑卡请求短验重发", YibaoApiServiceType.SYNC),
    YIBAO_BINDPAY_DIRECT("/bindpay/direct", "无短验充值",YibaoApiServiceType.ASYNC),
    YIBAO_BINDPAY_REQUEST("/bindpay/request", "有短验充值", YibaoApiServiceType.ASYNC),
    YIBAO_BINDPAY_CONFIRM("/bindpay/confirm", "有短验充值短验确认", YibaoApiServiceType.SYNC),
    YIBAO_BINDPAY_RESENDSMS("/bindpay/resendsms", "有短验充值请求短验重发", YibaoApiServiceType.SYNC),
    YIBAO_FIRSTPAY_REQUEST("/firstpay/request", "首次充值请求", YibaoApiServiceType.ASYNC),
    YIBAO_FIRSTPAY_CONFIRM("/firstpay/confirm", "首次充值短验确认", YibaoApiServiceType.SYNC),
    YIBAO_FIRSTPAY_RESENDSMS("/firstpay/resendsms", "首次充值短验重发", YibaoApiServiceType.SYNC),
    YIBAO_WITHDRAW_REQUEST("/withdraw/request", "提现请求", YibaoApiServiceType.ASYNC),
    YIBAO_BINDCARD_RECORD("/bindcard/record", "绑卡记录查询", YibaoApiServiceType.SYNC),
    YIBAO_BINDPAY_RECORD("/bindpay/record", "充值记录查询", YibaoApiServiceType.SYNC),
    YIBAO_WITHDRAW_RECORD("/withdraw/record", "提现记录查询", YibaoApiServiceType.SYNC),
    YIBAO_FIRSTPAY_RECORD("/firstpay/record", "首次充值记录查询", YibaoApiServiceType.SYNC),
    YIBAO_REFUND_RECORD("/refund/record", "退款记录查询", YibaoApiServiceType.SYNC),
    YIBAO_WITHDRAW_VALIDAMOUNT("/withdraw/validamount", "可提现余额查询", YibaoApiServiceType.SYNC),
    YIBAO_BINDCARD_LIST("/bindcard/list", "绑卡列表查询", YibaoApiServiceType.SYNC),
    YIBAO_BANKCARD_CHECK("/bankcard/check", "银行卡查询", YibaoApiServiceType.SYNC),
    YIBAO_REFUND_REQUEST("/refund/request", "退款请求", YibaoApiServiceType.SYNC),
    YIBAO_BINDCARD_UNIFIED("/bindcard/unified", "统一鉴权绑卡请求", YibaoApiServiceType.SYNC),
    YIBAO_BINDPAY_UNIFIED("/bindpay/unified", "统一支付请求", YibaoApiServiceType.SYNC),
    YIBAO_ACCOUNTCHECK_AUTH("/accountcheck/auth", "鉴权绑卡对接记录下载", YibaoApiServiceType.SYNC),
    YIBAO_ACCOUNTCHECK_PAYMENT("/accountcheck/payment", "支付队长文件下载", YibaoApiServiceType.SYNC),
    YIBAO_ACCOUNTCHECK_WITHDRAW("/accountcheck/withdraw", "提现对账文件下载", YibaoApiServiceType.SYNC),
    YIBAO_ACCOUNTCHECK_CHANGCARD("/accountcheck/changcard", "换卡记录下载", YibaoApiServiceType.SYNC),
    YIBAO_ACCOUNTCHECK_REFUND("/accountcheck/refund", "退款对账文件下载", YibaoApiServiceType.SYNC),
    ;

    private final String code;
    private final String message;
    private final YibaoApiServiceType apiServiceType;

    private YibaoServiceNameEnum(String code, String message, YibaoApiServiceType apiServiceType) {
        this.code = code;
        this.message = message;
        this.apiServiceType = apiServiceType;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public YibaoApiServiceType getApiServiceType() {
        return apiServiceType;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (YibaoServiceNameEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YibaoServiceNameEnum find(String code) {
        for (YibaoServiceNameEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YibaoServiceNameEnum> getAll() {
        List<YibaoServiceNameEnum> list = new ArrayList<YibaoServiceNameEnum>();
        for (YibaoServiceNameEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YibaoServiceNameEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
