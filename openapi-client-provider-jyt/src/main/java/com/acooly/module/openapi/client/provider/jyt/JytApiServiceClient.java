/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.jyt;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Strings;
import com.acooly.core.utils.net.HttpResult;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiClientSocketTimeoutException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.jyt.domain.JytNotify;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.exception.JytApiClientProcessingException;
import com.acooly.module.openapi.client.provider.jyt.marshall.*;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytHeaderRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * ApiService执行器
 *
 * @author zhangpu
 */
@Component("jytApiServiceClient")
@Slf4j
public class JytApiServiceClient
        extends AbstractApiServiceClient<JytRequest, JytResponse, JytNotify, JytNotify> {


    public static final String PROVIDER_NAME = "jyt";

    @Resource(name = "jytHttpTransport")
    private Transport transport;

    @Resource(name = "jytRequestMarshall")
    private JytRequestMarshall requestMarshal;

    @Resource(name = "jytRedirectMarshall")
    private JytRedirectMarshall redirectMarshal;

    @Resource(name = "jytSignMarshall")
    private JytSignMarshall signMarshall;

    @Resource(name = "jytRedirectPostMarshall")
    private JytRedirectPostMarshall jytRedirectPostMarshall;

    @Autowired
    private JytResponseUnmarshall responseUnmarshal;
    @Autowired
    private JytNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private OpenAPIClientJytProperties openAPIClientJytProperties;

    @Override
    public JytResponse execute(JytRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(openAPIClientJytProperties.getGatewayUrl());
            }
            //组装请求报文头
            JytHeaderRequest headerRequest = new JytHeaderRequest();
            headerRequest.setMerchantId(openAPIClientJytProperties.getPartnerId());
            headerRequest.setTranCode(JytServiceEnum.find(request.getService()).getKey());
            headerRequest.setTranFlowid(openAPIClientJytProperties.getPartnerId() + Ids.Did.getInstance().getId(20));
            request.setHeaderRequest(headerRequest);
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            HttpResult result = getTransport().request(requestMessage, url);
            if (result.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
                throw new RuntimeException("HttpStatus:" + result.getStatus());
            }
            if(Strings.isBlank(result.getBody())) {
                throw new ApiClientException("响应报文位空");
            }
            JytResponse t = getResponseUnmarshal().unmarshal(result.getBody(), request.getService());
            afterExecute(t);
            return t;
        } catch (JytApiClientProcessingException pe) {
            log.error("解析响应报文异常：" + pe.getMessage(), pe);
            throw pe;
        } catch (ApiClientSocketTimeoutException ase) {
            log.error("响应超时异常：" + ase.getMessage(), ase);
            throw new JytApiClientProcessingException(ase.getMessage());
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public JytNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String,String> notifyData = getSignMarshall().getDateMap(request);
            JytNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public PostRedirect redirectPost(JytRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = jytRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, JytRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<JytResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<JytNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, JytRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<JytNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    public JytSignMarshall getSignMarshall() {
        return signMarshall;
    }

    public void setSignMarshall(JytSignMarshall signMarshall) {
        this.signMarshall = signMarshall;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }
}
